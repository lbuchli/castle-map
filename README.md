# castle-map

Parf of the Castle Dossier Switzerland (Burgen-Dossier Schweiz) initiative of OpenStreetMap Switzerland and Wikimedia CH:

- [OSM wiki page](https://wiki.openstreetmap.org/wiki/Burgen-Dossier_Schweiz)
- [Wikimedia wiki page](https://meta.wikimedia.org/wiki/Wikimedia_CH/Burgen-Dossier)

## Dependencies 
* Wikidata
* Wikipedia
* Wikimedia Commons

Web service 'miniwfs' written in Go:
* Instance: https://castle-miniwfs.geoh.infs.ch
* Source code: https://github.com/brawer/miniwfs
* API / Routes of miniwfs:
  * /tiles/castles/{z}/{x}/{y}.png
  * /tiles/castles/${tileCoords.z}/${tileCoords.x}/${tileCoords.y}/${relativeOffset.x}/${relativeOffset.y}.geojson
  * /collections/castles/items/${featureId}
  * /collections/castles/items?bbox=${northEast.lng},${northEast.lat},${southWest.lng},${southWest.lat}&limit=${limit}

## Development Setup

Perform the following steps to set up the development environment:

1. Run `yarn install` to install the project's dependencies
2. Run `yarn start` to start a development server on [http://localhost:3000](http://localhost:3000).

## Production Build

Run the command `yarn build` to create a production build of the project in the `build` folder.

## Deployment via Docker

1. Build the Docker image: `docker build . -t castle-map`
2. Start a Docker container: `docker run --rm -p 3000:80 castle-map`
3. Open the website at [http://localhost:3000](http://localhost:3000).
