FROM node:8.15.1-alpine as builder
WORKDIR /home/node/app
COPY package.json yarn.lock ./
COPY public ./public
COPY src ./src
RUN yarn install
RUN yarn build

FROM nginx
ADD nginx-data-serve/default-site.conf /etc/nginx/conf.d/default.conf
COPY --from=builder /home/node/app/build /var/data/html
